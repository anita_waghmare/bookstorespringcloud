package com.vapasi.bookstore.bsgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
public class BsGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsGatewayApplication.class, args);
	}

}
